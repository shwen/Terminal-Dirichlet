import math
import json

from .navigation import ShortestPathFinder
from .util import send_command, debug_write
from .unit import GameUnit
from .game_map import GameMap

class replay:
    def __init__(self,config,replay_path):
        self.config=config
        self.replay_path=replay_path
        def replay_state():
            with open(self.replay_path) as f:
                data = f.readlines()
            initialstate=data[1]
            endstate = data[-1]
            return [eval(initialstate)]+[eval(endstate)]
        self.replay=replay_state(self.replay_path)
        global FILTER, ENCRYPTOR, DESTRUCTOR, PING, EMP, SCRAMBLER, REMOVE, UNIT_MAPPING
        FILTER = "FF"
        ENCRYPTOR = "EF"
        DESTRUCTOR = "DF"
        PING = "PI"
        EMP = "EI"
        SCRAMBLER = "SI"
        REMOVE = "RM"
        UNIT_MAPPING=[FILTER,ENCRYPTOR,DESTRUCTOR,PING,EMP,SCRAMBLER,RM]
        self.ARENA_SIZE = 28

def parse_replay_dict(self,index):
    result={"p1map":[],"p2map":[],"p1Stats":[],"p2Stats":[]}
    default1=GameMap(self.config)
    default2=GameMap(self.config)
    for i in range(len(self.replay[index]['p1Units'])):
        for j in self.replay['p1Units'][i]:
            default1.__map[j[0],j[1]].append(GameUnit(self,self.UNIT_MAPPING[i],self.replay,0,j[2],j[0],j[1]))
    for i in range(len(self.replay[index]['p2Units'])):
        for j in self.replay['p2Units'][i]:
            default2.__map[j[0],j[1]].append(GameUnit(self,self.UNIT_MAPPING[i],self.replay,1,j[2],j[0],j[1]))
    result["p1map"]=default1
    result["p2map"]=default2
    result["p1Stats"]=self.replay[index]["p1Stats"]
    result["p2Stats"]=self.replay[index]["p2Stats"]
    return result

def parse_replay(self):
    return [self.parse_replay_dict(self,0),self.parsel_replay_dict(self,1)]



